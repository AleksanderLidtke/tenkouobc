/*
 * File:   main.c
 * Author: Alek
 *
 * Created on May 25, 2018, 10:20 AM
 */

#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

#define _XTAL_FREQ 20000000 // 20 MHz crystal.

#include "TK-01-01-01_OBC.h"

__EEPROM_DATA(0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);

void main(void)
{
    __delay_ms(5000);
    
    resetCounterFunction ();
    
    /* Configure all pins as inputs and outputs appropriately. */
    spiMasterInit(); // Setup MISO, MOSI and SCLK.
    // Subsystem chip selects as outputs, subsystem->OBC notifications as inputs.
    SPI_BUS_CS_EPS_INIT = 0; SPI_BUS_CS_COMM1_INIT = 0; SPI_BUS_CS_COMM2_INIT = 0;
    GPIO_OBC_EPS_INIT = 1; GPIO_OBC_COMM1_INIT = 1; GPIO_OBC_COMM2_INIT = 1;
    // Enables for the level shifters and CSes for devices within the OBC PCB.
    RTC_I2C_ENABLE_INIT = 0; SD_SPI_ENABLE_INIT = 0;
    SPI_BUS_CS_SD_INIT = 0; SPI_BUS_CS_ADC_INIT = 0;
    // MCU-BCU interface.
    MCU_BCU_RESET_INIT = 0; // Pull low to reboot the other PIC.
    MISO_SEL_INIT=0; // Used to disconnect MISO of the SPI_BUS from the internal OBC SPI.
    PIC_SEL_INIT=0; // Chooses which of the two PICs is connected to the IOs.
    // Configure all pins as digital.
    ADCON1=ANALOGUE_PIN_SETUP;
    
    // Initialize UART pins when UART is not used!!
    TRISC7 = 0;
    TRISC6 = 0;
    
    /* Initialise the values of all pins. */
    // Don't communicate with the RTC or the SD card unless we want to.
    RTC_I2C_ENABLE = 0; SD_SPI_ENABLE = 0;
    // Initialise CSes - don't want slaves to read unless we deliberately lower the CS.
    SPI_BUS_CS_EPS = 1; SPI_BUS_CS_COMM1 = 1; SPI_BUS_CS_COMM2 = 1;
    SPI_BUS_CS_SD = 1;SPI_BUS_CS_ADC = 1;
    
    // CSes for the ADC and the SD card are defined in structs.
    tempADC.port =& PORTD; tempADC.pin = 3;
    tempADC.range_select = DOUBLE_RANGE; tempADC.outcoding_select = STRAIGHT_BINARY;
    
    //obcSDCard.port=&PORTD; obcSDCard.pin=2;
    //obcSDCard.data_size=4; obcSDCard.start_byte_number=0;

    //TODO decide which PIC is the master here.
    /* Choose whether to turn the other PIC on or off. 0 for off (disable).
     * Also choose which PIC is connected to the OBC I/Os. */
    MCU_BCU_RESET = 1; // Turn on the MCU.
    PIC_SEL = 1; // If or of this and the BCU is 0 => MCU is connected to I/Os.
    MISO_SEL = 1; // MSIO connected to the subsystems. Can be left like this
                  // with EPS and CCU1 connected.
    
    /* Initialise the digital communications. */
    //uartInit(UART_BAUD); // Initialise UART @ 9600 baud.
    i2cMasterInit(I2C_FREQ); // Start I2C @ 100 kHz frequency (supported by the RTC).
        
    __delay_us(10); // Arbitrary, let the RTC start up before resetting the time.

    //2. Set the time and date from SD card to RTC
    init_timing();

    __delay_ms(5000);   // Give enough time to EPS to initialize subsystems

    if (firstBootFlag != 0x00)      // Is it the 1st reset?
    {
        // Read EPS to know which cause the RESET
        ret = packACommand(C_OBC_TICK, 0, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);

        save_log_to_eeprom(bufferReceive[1]);    // Reset was by command
    }
    
   while(1) // Main loop.
    {
        switch(currentCommand[2])
        {
            case NORMAL_MODE:      // Normal mode
                currentMode = 0x30;
                ret = 0;

                for (uint8_t i = 30; i != 0; --i)
                {
                    timing_one_sec();

                    commFlag = readCommPins();

                    if (commFlag != 0)
                    {
                        ret = receiveGScommand (&commFlag);
                        commFlag = 0;
                        if (ret == 1) {break;}
                        else {memset(currentCommand, 0, sizeof(currentCommand)); i = 30;}
                    }
                }

                if (ret == 1) {break;}

                sendBeacon ();

                telemetryFlag++;

                if (telemetryFlag == 10)    // After 10 minutes, save telemetry data in EPS and IFPV
                {
                    // Save EPS telemetry in SD card
                    ret = packACommand(D_OBC_SD_TEST, 0, currentTime_pic, 7);
                    ret = exchangeDataI2C(&picBAddr, 9);

                    __delay_ms(100);

                    // TODO: Send command for IFPV telemetry DL

                    telemetryFlag == 0;
                }

                break;

            case MISSION_NORMAL_MODE:      // Perform normal mission mode
                currentMode = currentCommand[3];
                beaconCounter = 50;
                ucpFlag = 20;
                missionStartTime = (((uint16_t)currentCommand[4]) << 8) + currentCommand[5];      // Mission 1 start time
                partialTime_1 = (((uint16_t)currentCommand[6]) << 8) + currentCommand[7];         // Mission 1 ending time
                partialTime_2 = (((uint16_t)currentCommand[8]) << 8) + currentCommand[9];         // Mission 2 start time
                missionFinishTime = (((uint16_t)currentCommand[10]) << 8) + currentCommand[11];   // Mission 2 ending time
                adsTime = (((uint16_t)currentCommand[12]) << 8) + currentCommand[13];             // ADS start and finish time

                for (uint8_t z = 0; z < 22; z++)    // Make configuration packet for mission with RTC time just the 1st time
                {
                    currentCommand[42 - z] = currentCommand[36 - z];    // Shift currentCommand 6 positions for current time mission start
                }

                for (uint16_t i = (missionFinishTime + adsTime); i != 0 ; --i)    // Perform mission from 0 to Mission 2 ending time plus ADS time
                {
                    timing_one_sec();   // Count 1 second

                    currentMissionTime++;   // Increase mission counter
                    beaconCounter++;        // Increase beacon counter
                    
                    if (beaconCounter == 60) { sendBeacon (); beaconCounter = 0;}   // Send beacon every minute
                    
                    if ( (ucpFlag == 60) && ((currentCommand[3] == 0x38) || (currentCommand[3] == 0x39)) )
                    {
                        // Read ECU for check status
                        ret = packACommand(C_OBC_TICK, 0x00, currentTime_pic, 6);
                        ret = exchangeDataI2C(&uecuAddr, 8);
                        
                        __delay_ms(10);
                        
                        ret = packACommand(0xC9, currentCommand[3], bufferReceive+1, bufferReceive[0]);
                        ret = exchangeDataI2C(&picBAddr, bufferReceive[0]+2);                      
                        
                        ucpFlag = 0;
                    } 
                        
                    if ((currentMissionTime >= (missionStartTime - adsTime)) && currentCommand[3] < 37)   // Is time to start ADS mode and is this a command includes ADS?
                    {
                        if (adsInitFlag == 0)   // First time ADS booting
                        {
                            if ((currentCommand[3] >= 0x31) && (currentCommand[3] <= 0x33)) { adsTurnOn (2); }        //  Turn on PICSENSE and ADS lines
                            else if ((currentCommand[3] >= 0x34) && (currentCommand[3] <= 0x36)) { adsTurnOn (1); }   //  Turn on PICSENSE only
                            //__delay_ms(4500);   // Is it necessary this big?
                            __delay_ms(100);

                            if (currentCommand[39] == 0x01)   // ADS SD card backup mode (IF AUTOMATIC SCAN SECTOR FAILS)
                            {
                                //Send additional backup parameters to IF_PV
                                ret = packACommand(0x13, currentCommand[40], currentCommand+41, 2);
                                ret = exchangeDataI2C(&snseAddr, 4);
                                __delay_ms(20);
                            }

                            // Start ADS normal mode
                            ret = packACommand(0x30, 0x01, currentTime_pic, 7);
                            ret = exchangeDataI2C(&snseAddr, 9);

                            for (int j = 2; j != 0; --j)    // wait 2 seconds for IF_PV correct functionality
                            {
                                timing_one_sec();           // Count 1 second
                                currentMissionTime++;       // Increase mission counter
                            }

                            adsInitFlag = 1;    // Finish ADS booting
                        }

                        //Send tick to ADS for perform a valid reading
                        ret = packACommand(0x30, 0x01, currentTime_pic, 7);
                        ret = exchangeDataI2C(&snseAddr, 9);
                    }

                    if ((currentMissionTime == missionStartTime) || (currentMissionTime == partialTime_2))  // Is it time to start a mission?
                    {
                        memcpy(currentCommand+15, currentTime_pic, 6);  // Add current time for mission configuration packet
                        
                        switch (currentCommand[3])    // Which mission will be performed?
                        {
                            case 0x31: case 0x34: case 0x40:                   // DLP Normal mode
                                dlpNormalMode (currentCommand+14);
                                break;

                            case 0x32: case 0x35: case 0x41:
                                cpdNormalMode (currentCommand+14);              // CPD Normal mode
                                break;

                            case 0x33: case 0x36: case 0x42:
                                cpdAndDlpNormalMode (currentCommand+14);        // CPD+DLP Normal mode
                                break;

                            case 0x37:                                          // Material mission mode
                                materialNormalMode ();
                                break;      
                            
                            case 0x38:                                          // UCP mission mode
                                ucpNormalMode ();
                                break;
                            
                            case 0x39:                                          // Thermal switch mission mode
                                thermalSwitchNormalMode ();
                                break;
                            
                            default:
                            break;
                        }
                    }

                    if ((currentMissionTime == partialTime_1) || (currentMissionTime == missionFinishTime))   // Is it time to finish mission?
                    {
                        switch (currentCommand[3])    // Which mission will be finish for turn it off?
                        {
                            case 0x31: case 0x34: case 0x40: 
                                payloadsTurnOnOff (C_SETOFF, 3);     // Turn OFF DLP+PL
                                break;

                            case 0x32: case 0x35: case 0x41:
                                payloadsTurnOnOff (C_SETOFF, 2);     // Turn OFF CPD and PL
                                break;

                            case 0x33: case 0x36: case 0x42:
                                payloadsTurnOnOff (C_SETOFF, 4);     // Turn OFF CPD, DLP and PL
                                break;
                            
                            case 0x37:                 
                                payloadsTurnOnOff (C_SETOFF, 1);     // Turn OFF DLP+PL
                                break;  
                                
                            case 0x38: case 0x39:
                                ret = packACommand(C_SETOFF, UCP_SW, noExtraData, 0);   // Turn OFF UCP
                                ret = exchangeDataI2C(&picBAddr, 2);

                            default:
                                break;
                        }
                    }

                }   // Complete mission time elapsed

                timing_one_sec();
    
                if (adsInitFlag == 1)
                {
                    //Send command to IF_PV for saving last SD address
                    ret = packACommand(0x14, 0x00, currentTime_pic, 7);
                    ret = exchangeDataI2C(&snseAddr, 9);
                
                    timing_one_sec();
                
                    adsTurnOff (2);             // End ADS mission (turn it off)
                }
                
                currentMissionTime = 0;     // Reset mission time
                adsInitFlag = 0;            // Clear ADS boot flag
                ucpFlag = 20;               // Clear UCP flag
                beaconCounter = 0;          // Clear beacon counter
                currentCommand[2] = CLEAR_BUFFER;   // Exit to clear buffer state
                break;

            case TELEMETRY_MODE:      // Request Telemetry packet
                requestTelemetryPacket (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case ADS_REALTIME_MODE:      // ADS real time data request
                IFPVrealDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case EPS_REALTIME_MODE:      // EPS real time data request
                EPSrealDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case ADS_SDCARD_MODE:      // ADS SD card data request
                IFPVsdCardDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case DLP_REALTIME_MODE:      // DLP real time Data request
                DLPrealTimeDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case CPD_REALTIME_MODE:      // CPD real time data request
                CPDrealTimeDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case LIULIN_REALTIME_MODE:      // Liulin real data request
                LiulinRealTimeDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case MATERIAL_REALTIME_MODE:
                materialRealTimeDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case SDCARD_MODE:      // EPS SD card data request
                sdCardDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case DIRECT_SUBSYSTEM_COMMAND:      // For sending direct command from OBC to any subsystem via I2C
                directSubsystemCommand (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case LOG_REQUEST:
                logDataRequest (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case CPD_NORMAL_MODE:
                cpdNormalMode (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case DLP_NORMAL_MODE:
                dlpNormalMode (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case CPD_DLP_NORMAL_MODE:
                cpdAndDlpNormalMode (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case MATERIAL_NORMAL_MODE:
                materialNormalMode ();
                currentCommand[2] = CLEAR_BUFFER;
                break;
                
            case MONITOR_MODE:
                monitorMode (currentCommand+3);
                currentCommand[2] = CLEAR_BUFFER;
                break;

            case CLEAR_BUFFER:
                memset(currentCommand, 0, sizeof(currentCommand));
                break;

            default:
                break;
        }
    }
}
# TenkouOBC

This is a repository for software development of *TK-01-01-01 OBC* board for Ten-Koh mission. It contains two projects, one for each
PIC located within the OBC PCB as well as the code that is shared between them.

# Folder structure

## OBC

This repository contains projects for two PICs, the main controller unit, *MCU* and the backup controller unit, the *BCU*. Each of the
projectsis located in a different folder to avoid having to switch repository branches or use compiler pragmas to select code for each
PIC (each PIC needs a different `main` function, and only one main is allowed in a project). The code shared between the two PICs,
including the `TK-01-01-01_OBC.h` file that contains the pin definition of the two PICs, is located in a separate folder called *shared*.

**NOTE** The *shared* folders contains definitions of the I2C addresses of all the PICs (*I2CAddresses.h*) as well as
definitions of all the on-board commands and their format (*OnBoardCommands.h*). These files should be included by
all the PIC software projects to ensure consistency in command and address definitions.

## Libraries

The PIC projects in this repository use several Tenkou software libraries that are shared with other subsystems. To make sure that these
libraries are found where MPLAB expects them (and to avoid having to change the project `configuration.xml` all the time), please clone the
shared libraries into folders with the following names (defaults used by git):

* tenkouadc,
* tenkouobc,
* tenkougpio,
* tenkoui2cmaster,
* TenkouSDCard,
* tenkouspimaster,
* tenkourtc,
* tenkouuart.

For a list of the owners of these repositories, please check this [Google Drive Sheet](https://docs.google.com/spreadsheets/d/1mfUhjpLLei_G4BwL3_K3ghEvJ2L69L-y8f4Ou1xm3Q0/edit?usp=sharing).

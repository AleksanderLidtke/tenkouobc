/* This header file defines all the I2C slave address under which different
 * PICs on-board the Ten-Koh satellite are registered.
 */
 #ifndef I2C_ADDRESSES
 #define I2C_ADDRESSES
 
#define I2C_FREQ 100000 // I2C frequency in Hz. Should be the same for all the slaves.

 // Can only use 4 MSB bits in slave addresses, the last 4 bits have to be 0.
unsigned char picMAddr = 0x10; // I2C address of EPS PIC_M.
unsigned char picBAddr = 0x20; // I2C address of EPS PIC_B.
unsigned char ccu1Addr = 0x30; // I2C address of COMM 1 CCU1.
unsigned char ccu2Addr = 0x40; // I2C address of COMM 2 CCU2.
unsigned char uecuAddr = 0x50; // I2C address of Ultracapacitor experiment control unit.
unsigned char ecuMAddr = 0x60; // I2C address of Experiment control unit master.
unsigned char ecuBAddr = 0x70; // I2C address of Experiment control unit backup.
unsigned char snseAddr = 0x80; // I2C address of PIC_SENSE.
unsigned char matsAddr = 0x90; // I2C address of the Material mission control unit.

// For testing and debugging purposes, we may want an external PIC to be the I2C
// Master and the OBC PICs to be slaves just running test functions when
// instructed. Here are the allocated I2C addresses for the MCU and the BCU.
//unsigned char mcuDebugAddr = 0xB0;
//unsigned char bcuDebugAddr = 0xF0;

 #endif // Header safegurad.

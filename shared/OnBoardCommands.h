/* This header file defines all the command bytes that will be sent to different
 * Ten-Koh subsystems to produce certain effects.
 */
 #ifndef ON_BOARD_COMMANDS
 #define ON_BOARD_COMMANDS

/* Ground Station Commands */
// Mission modes
#define NORMAL_MODE                     0x00
#define CLEAR_BUFFER                    0xCC
#define TELEMETRY_MODE                  0x60
#define ADS_REALTIME_MODE               0x61
#define EPS_REALTIME_MODE               0x62
#define DLP_REALTIME_MODE               0x63
#define CPD_REALTIME_MODE               0x64
#define LIULIN_REALTIME_MODE            0x65
#define ADS_SDCARD_MODE                 0x66
#define SDCARD_MODE                     0x68
#define MISSION_NORMAL_MODE             0x69
#define DIRECT_SUBSYSTEM_COMMAND        0x70
#define LOG_REQUEST                     0x71
#define CPD_NORMAL_MODE                 0x72
#define DLP_NORMAL_MODE                 0x73
#define CPD_DLP_NORMAL_MODE             0x74
#define MATERIAL_REALTIME_MODE          0x75
#define MATERIAL_NORMAL_MODE            0x76
#define MONITOR_MODE                    0x77

// OBC Tick
#define C_OBC_TICK  0x21 // Send the data currently in the transmission buffer, i.e. status by default.
                         // This tick will be sent to all subsystems in sequence
                         // to synchronise all the PICs and supply them with the
                         // current OBC time in four bytes:
                         // seconds, minutes, hours and date, respectively.

/* EPS Controller commands. */
// Main commands.
//#define C_SENDHK    0x22 // Acquire housekeeping data and load into the transmission buffer.
#define C_SETON     0x22 // Turn on one power line specified in supplementary command data.
#define C_SETOFF    0x23 // Turn off one power line specified in supplementary command data.
#define C_SETRST    0x24 // Hard-reset a power line specified in supplementary command data.
#define C_SET_MODE  0x25 // Set the power line status to perform a given mission.
                         // The mode to engage is specified in supplementary command data.
#define C_SENDDH    0x26 //TODO send the housekeeping data history from the SD card.
               //TODO this'll be tricky, we have to think how to implement it.
               //TODO It'll require specifying SD card locations to send to the OBC.
#define C_KILL      0x27 // Set the KILL_SWITCH in a reset position, which kills Ten-Koh.
#define C_NO_KILL   0x28 // Set the KILL_SWITCH in a SET position, which keeps Ten-Koh alive.
#define C_TM_FREQ   0x29 // Define how often (every how many main loops) to save
                         // the housekeeping data to the SD card. Frequency is
                         // specified in the supplementary data.

// Supplementary data for certain commands. Specify power lines and things that
// work in the same way, i.e. can be turned on, off or reset with C_SETON,
// C_SET_OFF and C_SETRST commands.
#define OBC_SW      0x20 // 5V_OBC line - OBC will only reboot itself if the RTC
                         // hangs. We can also do it from the outside PIC for testing.
#define COM1_SW     0x21 // 5V_COM1 line.
#define COM2_SW     0x22 // 5V_COM2 line.
#define UCP_SW      0x23 // 5V_UCP line.
#define PL_SW       0x24 // 5V_PL line.
#define DLP_SW      0x25 // 5V_DLP line.
#define CPD_SW      0x26 // 12V_CPD line.
#define ADS_SW      0x27 // 12V_ADS line.
#define HEAT_SW     0x28 // Heater enable. Not a power line but can be used to enable/disable the heaters.
#define PIC_SENSE   0x29 // PIC_SENSE MCLR. Can be used to turn the PIC on, off or reset it.
#define SD_CARD     0x2A // Whether to save housekeeping data to the SD card or not. Yes by default. Cannot be reset.
#define HEATER1_SW  0x2B // Heater 1 line 
#define HEATER2_SW  0x2C // Heater 2 line 

// Debugging commands - not for flight, OBC will never send them to the EPS.
#define D_SATRST    0x90 // Power reset all power lines and return to nominal mode.
                         // This is normally executed after a SAT_RESET GPIO is
                         // set high by one of the CCUs, but we want to be able
                         // to debug this from the outside PIC by sending a command.
#define D_GPIO_OBC  0x91 // Set the OBC interrupt status high or low, as specified in the supplementary data.
#define D_EPS_IFSWADC 0xD0  //Read ADC of IFSW
#define D_EPS_OCPADC 0xD1   //Read ADC from EPS controller
#define D_EPS_RDSBUSADC 0xD2//Read ADC from BUS
#define D_EPS_RDSPLADC 0xD3 //Read ADC from PL
#define D_EPS_GPIOCOM 0xD4  //Read the GPIO from COM


/* COMM commands. */
#define C_TRCW      0x30 // Send CW using COMM. Needs the data to send with the beacon in extra command data.
#define C_TRPACK    0x31 // Send package using COMM. Needs the data to send in extra command data.
#define C_TRACK     0x32 // Send acknoledge package to GS. Need an acknoledge package in extra command data.
#define C_TRON      0x33 // Turn ON COMM TX.
#define C_TROFF     0x34 // Turn OFF COMM TX.
#define C_TRSNG     0x33 //TODO confirm with Rafa. Cause the CCU to put the TX in audio mode and power on the SNG circuit.

/* OBC debugging commands - not for flight, just to test the OBC PCB
 * functionality from outside, with the OBC PICs registered as I2C slaves.
 * Each PIC, MCU and BCU, accepts the same commands and it's up to the user to
 * configure one of them as the master by turning the other PIC off and adjusting
 * PIC_SEL. */
#define D_OBC_PIC_SEL 0x9A // Set PIC_SEL high or low depending on the SUP byte -
                           // 1 for high, 0 for low. Set high for BCU to be the master.
#define D_OBC_PIC_RST 0x9B // Turn the PIC on or off depending on the SUP byte -
                           // 1 for on, 0 for off.
#define D_OBC_RTC_EN  0xAF // Set RTC_I2C_ENABLE high or low depending on the SUP byte -
                           // 1 for high, 0 for low. Set high to communicate with the RTC.
#define D_OBC_SD_TEST 0x9C // Run the SD card test (write and read known data)
                           // and load them into the transmission buffer so that
                           // we can verify them.
#define D_OBC_T_TEST  0x9D // Read the temperatures of MCU and BCU, and load them
                           // into the transmission buffer so that we can verify them.
#define D_OBC_RD_INT  0x9E // Read the GPIO status pins of EPS Controller, CCU1
                           // and CCU2. Load them into the transmission buffer
                           // so that we can read and verify them.
#define D_OBC_SND_SPI 0x9F // Send the OPT data to a given subsystem (given by SUP
                           // byte), read the reply and load it into the transmission
                           // buffer so that we can read and verify the reply.
// Material mission commands
#define MM_STAND_BY     0x55

/* IF_PV commands*/
//      C_OBC_TICK       0x21 //Return 11111
#define C_SOL_PAN_1_6    0x22 //Return values from 1-6 solar panels
#define C_SOL_PAN_7_12   0x23 //Return values from 7-12 solar panels
#define C_SUN_SEN_1_6    0x24 //Return values from 1-6 sun panels
#define C_SUN_SEN_7_12   0x25 //Return values from 1-6 sun panels
#define C_GYROS          0x26 //Return values from 3 gyros (x,y,z,temp)
#define C_MAGNETOMETER   0x27 //Return values from magnetometer
#define C_SD_CARD        0x28 //Return values from SD card (888888 - success read and write, AAAAAA - not success)
#define C_HARNESSES      0x29 //Send clock signals for harnesses check

#endif // Header safeguard.

/* This files defines all the functions shared between the main controller unit
 * (MCU) and the backup controller unit (BCU) of the Ten-Koh satellite, which
 * are both housed within the TK-01-01-01 OBC PCB.
 *
 * Author: Alek, Rafael
 */
#include "TK-01-01-01_OBC.h"

uint8_t measureTemperature(uint16_t* data)
/* Read the temperatures of the MCU and the BCU and store them in the data array.
 * Return 1 at the end. */
{
    // Read the ADC - MCU T (IC10 on the PCB) on channel 1, BCU T (IC12) on channel 2.
    for(unsigned char i = 0; i < 2; i++)
    {
        tempADC.channel_select = i;
        data[i] = adc_communication_normal(tempADC);
        data[i] = data[i] & 0x0FFF;
    }
    return 1;
}

uint8_t readCommPins(void)
/* Read the status pins of COMM1, COMM2 and EPS. Return depending on which of
 * them is read as high:
 * - 0 if none,
 * - 1 for CCU1 (uplink data received),
 * - 2 for CCU2 (uplink data received),
 */
{
    if(GPIO_OBC_COMM1 == 0)
    {
        return ccu1Addr;
    }

    if(GPIO_OBC_COMM2 == 0)
    {
        return ccu2Addr;
    }

    return 0; // Haven't read any GPIOs as high yet, so all of them are low.
}

uint8_t exchangeDataI2C(unsigned char* const slaveAddr, uint8_t noBytes)
/* Exchange data with a given slave over I2C: transmit the first noBytes from
 * bufferTransmit and write all the bytes received from the slave into bufferReceive.
 * slaveAddr specifies the I2C slave address with which we'll communicate with.
 * Return 1 if everything goes OK, 0 if the I2C transmission times out.
 *
 * Read the bytes from the slave first and then transmit data to it - the data
 * from the OBC will most often be commands and we have to make sure that the
 * slave transmits its own data before executing the OBC commands.
 */
{
    bufferTransmit[noBytes] = 0xFF; // Mark the end of data - can't be the last byte of the array.
    memset(bufferReceive, 0, RECEIVE_BUF_SIZE); // Will only have the received data here afterwards.

    // First read from the slave.
    ret = i2cMasterStart(); // Starts master communication by I2C.
    if(ret!=1){return 0;}; // Something went wrong.
    ret = (uint8_t) i2cMasterReadBytesPIC(slaveAddr,bufferReceive);
    ret = i2cMasterStop(); // Stops communications by I2C even if slave times out not to hang the bus.
    if(ret!=1){return 0;}; // Something went wrong.

    // Then transmit to it.
    ret = i2cMasterStart(); // Issue START condition to start I2C comms.
    if(ret!=1){return 0;}; // Something went wrong.
    ret = (uint8_t) i2cMasterWriteBytesPIC(slaveAddr,bufferTransmit,TRANSMIT_BUF_SIZE);
    ret = i2cMasterStop(); // Issue STOP condition even if slave times out not to hang the bus.
    if(ret!=1){return 0;}; // Something went wrong.

    return 1;
}

uint8_t exchangeDataSPI(uint8_t slaveIndex, uint8_t noBytes)
/* Exchange data with a given slave over SPI: transmit the first noBytes from
 * bufferTransmit and write all the bytes received from the slave into bufferReceive.
 * Only three SPI slaves are connected to the OBC and slaveIndex specifies which
 * one we'll communicate with:
 * 0 - CCU1,
 * 1 - CCU2,
 * 2 - EPS.
 * Return 1 if everything goes OK.
 *
 * Read the bytes from the slave first and then transmit data to it - the data
 * from the OBC will most often be commands and we have to make sure that the
 * slave transmits its own data before executing the OBC commands.
 */
{
    bufferTransmit[noBytes] = 0xFF; // Mark the end of data - can't be the last byte of the array.
    memset(bufferReceive, 0, RECEIVE_BUF_SIZE); // Will only have the received data here afterwards.

    MISO_SEL = 0; // Connect the MISO to the subsystems.
    SPI_MOSI_MASTER = 0; // These are set in the read/write functions, but
                         // PIC-to-PIC SPI doesn't work without setting them here.
    SPI_CK_MASTER = 0;

    // CS low for the given slave.
    switch(slaveIndex){
        case 0: SPI_BUS_CS_COMM1 = 0; break;
        case 1: SPI_BUS_CS_COMM2 = 0; break;
        case 2: SPI_BUS_CS_EPS = 0; break;
    }

    // First read from slave, then send data to it.
    __delay_ms(1); // Give the slave some time to count how many bytes it'll send
                   // after the CS and before the clock.
    spiMasterReadBytesPIC(bufferReceive); // Receive the data sent by the slave.
    __delay_ms(1); // Probably don't need to wait before reading from and writing
                   // to the slave but just to be safe. 100 ms is slave's timeout limit.
    spiMasterWriteBytesPIC(bufferTransmit,TRANSMIT_BUF_SIZE); // Slave should print this on UART.

    // CS back high.
    switch(slaveIndex){
        case 0: SPI_BUS_CS_COMM1 = 1; break;
        case 1: SPI_BUS_CS_COMM2 = 1; break;
        case 2: SPI_BUS_CS_EPS = 1; break;
    }

    MISO_SEL = 1; // No longer need to communicate with the subsystems on SPI, disconnect.

    return 1;
}

uint8_t packACommand(unsigned char commandByte, unsigned char suppDataByte,
        unsigned char* extraData, unsigned char extraDataSize)
/* Pack a command into the bufferTransmit. The command has the following bytes:
 * 0 - command byte (CMD),
 * 1 - supplementary command byte (SUP) that specifies details of the command,
 * 2 to TRANSMIT_BUF_SIZE-1 - optional data (OPT). If extraDataSize is 0,
 * no extraData will be appended to the command.
 *
 * To allow iteration over extraData, pass its length (extraDataSize) to this
 * function.
 *
 * Need to be careful not to exceed the transmitBuffer size with extraData.
 * Size of extraData must be at most TRANSMIT_BUF_SIZE-2 to allow for the
 * commandByte and the suppDataByte that will be put in the beginning of the
 * bufferTransmit.
 *
 * Return 1 if everything goes OK, 0 if the size of extraData exceeds the
 * available size of bufferTransmit accounting for the command and supplementary
 * command bytes.
 *
 * Example uses:
 *  - packACommand(C_SENDHK,0,noExtraData,0); // No suppDataByte or extraData.
 *  - packACommand(C_OBC_TICK,0,currentTime,4); // No suppDataByte.
 *  - packACommand(C_SETON,OBC_SE,noExtraData,0); // No extraData.
 */
{
    memset(bufferTransmit, 0, TRANSMIT_BUF_SIZE); // Make sure we don't send any unintended data.
    bufferTransmit[0] = commandByte;
    bufferTransmit[1] = suppDataByte;

    for(uint8_t i = 0; i < extraDataSize; i++) // Put extraData in transmit buffer.
    {
        // Check for memory access violations. The actual condition to check
        // is: (i+2 >= TRANSMIT_BUF_SIZE-1), i.e. whether we'll exceed
        // maximum index of bufferTransmit with the current i.
        if( i+3 >= TRANSMIT_BUF_SIZE)
        {
            return 0; // :(
        }

        bufferTransmit[i+2] = extraData[i]; // Just copy extra data after bytes 0 and 1.
    }

    return 1; // Done.
}

uint8_t receiveGScommand (const unsigned char* ccuAddress)
{
    uint8_t commandFlag = 0;

    //Get data requested data from EPS
    ret = packACommand(C_OBC_TICK, 0, noExtraData, 0);
    ret = exchangeDataI2C(ccuAddress,  2);

    __delay_ms(500);

    memcpy(currentCommand, bufferReceive, bufferReceive[0] - 1);                    // Save received command in a temporary array
    memset(bufferReceive, 0, RECEIVE_BUF_SIZE);                             // Erase bufferReceive

    if ((0x60 <= currentCommand[2]) && (currentCommand[2] <= 0x77))
    {
        memcpy(bufferReceive, "ACK:", 4);                                   // Make ACK package including valid command received
        save_log_to_eeprom(currentCommand[2]);                              // Save received command to LOG
        commandFlag = 1;
    }

    else    // Is it invalid command?
    {
        memcpy(bufferReceive, "NAK:", 4);                                   // Make NACK package including invalid command received
    }

    memcpy(bufferReceive+4, currentCommand+2, currentCommand[0]-3);

    ret = packACommand(C_TRACK, 0x01, bufferReceive, currentCommand[0]+1);  // Sending answer packet
    ret = exchangeDataI2C(ccuAddress, currentCommand[0]+3);

    __delay_ms(1000);
    
    ret = packACommand(C_TRACK, 0x01, bufferReceive+2, bufferReceive[0]-3);  // Sending answer packet another time
    ret = exchangeDataI2C(ccuAddress, bufferReceive[0]-1);
    
    __delay_ms(1000);

    for (uint8_t i = 4; i != 0; --i)
    {
        if (readCommPins() == 0x00) {break;}

        ret = packACommand(C_TRACK, 0x00, noExtraData, 0);  // Clear CCU1 command indication
        ret = exchangeDataI2C(&ccu1Addr, 2);

        __delay_ms(100);

        ret = packACommand(C_TRACK, 0x00, noExtraData, 0);  // Clear CCU1 command indication
        ret = exchangeDataI2C(&ccu2Addr, 2);

        __delay_ms(100);
    }

    return commandFlag;
}

void sendBeacon (void)
{
    ret = measureTemperature(currentTemp); // Check MCU and BCU temp.

    //Send beacon data request to EPS
    ret = packACommand(D_EPS_IFSWADC, 0, currentTime_pic, 7);
    ret = exchangeDataI2C(&picBAddr, 9);

    __delay_ms(200);    // Calculate reading time

    //Get data requested data from EPS
    ret = packACommand(C_OBC_TICK, 0, currentTime_pic, 7);
    ret = exchangeDataI2C(&picBAddr, 9);

    utoa (bufferReceive+bufferReceive[0]+1, currentTemp[0], 16);
    utoa (bufferReceive+bufferReceive[0]+4, currentTemp[1], 16);
    bufferReceive[bufferReceive[0]+7] = currentMode;

    if(ccuBeaconFlag == ccu1Addr)                      // It will starts sending by CCU2
    {
        bufferReceive[bufferReceive[0]+8] = 0x32;
        ccuBeaconFlag = ccu2Addr;
    }

    else
    {
        bufferReceive[bufferReceive[0]+8] = 0x31;
        ccuBeaconFlag = ccu1Addr;
    }

    //Sent data to CCU by beacon
    ret = packACommand(C_TRCW, 0, bufferReceive+1, bufferReceive[0]+8);
    ret = exchangeDataI2C(&ccuBeaconFlag, bufferReceive[0]+10);
}

void requestTelemetryPacket (const unsigned char* missionParameters)
{
    //Sent data to CCU by packet
    ret = packACommand(C_TRPACK, 0x01, currentTime_pic, 6);
    ret = exchangeDataI2C(&missionParameters[0], 8);

    __delay_ms(500);

    if (missionParameters[6] != 0)
    {
        for (unsigned char i = 0; i < 6; i++)
        {
            currentTime_pic[i] = missionParameters[i + 1];    //[0] - sec, [1] - min, [2] - hour, [3] - day, [4]- month, [5] - year
        }

        set_time_to_RTC();
    }

    adsTurnOn (1);

    ret = measureTemperature(currentTemp); // Check MCU and BCU temp.
    
    //uartWriteBytes(currentTemp, sizeof(currentTemp));

    //Send telemetry request to IF_PV
    ret = packACommand(0x31, 0, currentTime_pic, 7);
    ret = exchangeDataI2C(&snseAddr, 9);

    __delay_ms(500);

    //Get data requested data from IF_PV (PACKET 1)
    ret = packACommand(C_OBC_TICK, 0, currentTime_pic, 7);
    ret = exchangeDataI2C(&snseAddr, 9);

    __delay_ms(500);

    //Sent data to CCU1 by packet
    ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
    ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);

    __delay_ms(500);

    //Get data requested data from IF_PV (PACKET 2)
    ret = packACommand(C_OBC_TICK, 0, currentTime_pic, 7);
    ret = exchangeDataI2C(&snseAddr, 9);

    __delay_ms(500);

    //Sent data to CCU1 by packet
    ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
    ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);

    __delay_ms(10);

    //Send telemetry request to EPS
    ret = packACommand(0xE1, 0, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr,2);

    __delay_ms(490);    // Calculate reading time

    //Get data requested data from EPS
    ret = packACommand(C_OBC_TICK, 0, noExtraData, 0);
    ret=exchangeDataI2C(&picBAddr,2);
    
    //uartWriteBytes(bufferReceive, bufferReceive[0]);

    bufferReceive[bufferReceive[0]+1] = (currentTemp[0] & 0x0F00) >> 8;
    bufferReceive[bufferReceive[0]+2] = (currentTemp[0] & 0x00FF);
    bufferReceive[bufferReceive[0]+3] = (currentTemp[1] & 0x0F00) >> 8;
    bufferReceive[bufferReceive[0]+4] = (currentTemp[1] & 0x00FF);
    bufferReceive[bufferReceive[0]+5] = resetCounter[1];
    bufferReceive[bufferReceive[0]+6] = resetCounter[0];

    __delay_ms(500);      // Calculate waiting time for send the beacon

    //Sent data to CCU1 by packet
    ret=packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]+6);
    ret=exchangeDataI2C(&missionParameters[0], bufferReceive[0]+8);

    __delay_ms(500);    // Calculate reading time

    adsTurnOff (1);
}

void IFPVrealDataRequest (const unsigned char* missionParameters)
{
    adsTurnOn (2);

    if (missionParameters[2] == 0x01)
    {
        //Send data request to IF_PV
        ret = packACommand(0x13, missionParameters[3], missionParameters+4, 2);
        ret = exchangeDataI2C(&snseAddr, 4);
        __delay_ms(20);
    }

    //Send data request to IF_PV
    ret = packACommand(0x30, 0x02, currentTime_pic, 7);
    ret = exchangeDataI2C(&snseAddr, 9);

    __delay_ms(2000);

    for (unsigned char i = missionParameters[1]; i != 0 ; --i)
    {
        timing_one_sec();

        //Send data request to IF_PV
        ret = packACommand(0x30, 0x02, currentTime_pic, 7);
        ret = exchangeDataI2C(&snseAddr, 9);

        __delay_ms(10);

        if (i > 1)
        {
            // Sent data to CCU by packet
            ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }

        else
        {
            // Sent last data to CCU by packet
            ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }
    }

    timing_one_sec();
    
    //Send command to IF_PV for saving last SD address
    ret = packACommand(0x14, 0x00, currentTime_pic, 7);
    ret = exchangeDataI2C(&snseAddr, 9);
    
    timing_one_sec();

    adsTurnOff (2);

    TMR0_count_1sec = 0;
}

void EPSrealDataRequest (const unsigned char* missionParameters)
{
    //Send data request to EPS
    ret = packACommand(0xE1, 0, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);

    packetDownloadDelay (missionParameters[2]);

    for (unsigned char i = missionParameters[1] - 1; i != 0 ; --i)
    {
        //Send data request to EPS
        ret = packACommand(0xE1, 0, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);

        packetDownloadDelay (missionParameters[2]);
        
        // Sent data to CCU by packet
        ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
        ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        
        __delay_ms(50);
    }
    
    //Send data request to EPS
    ret = packACommand(C_OBC_TICK, 0, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);

    packetDownloadDelay (missionParameters[2]);

    // Sent last data to CCU by packet
    ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
    ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
 }

void IFPVsdCardDataRequest (const unsigned char* missionParameters)
{
    unsigned char sector_num, amount_of_data, parts_get;
    unsigned int start_data_num = 0;
    unsigned char getSecondData = 0;
    unsigned char ExtraData[5] = {0};

    adsTurnOn (1);

    sector_num = missionParameters[1];
    start_data_num = (((unsigned int)missionParameters[2]) << 8) + missionParameters[3];
    amount_of_data = missionParameters[4];
    parts_get = missionParameters[5];

    ExtraData[4] = 0x77;

    ret=packACommand(0x41, 0, ExtraData, 5);
    ret=exchangeDataI2C(&snseAddr, 7);

    __delay_ms(1000);

    if (parts_get)
    {
        for (unsigned int i = start_data_num; i < (start_data_num  + amount_of_data); i++)
        {
            ExtraData[0] = sector_num;
            ExtraData[1] = (i & 0xFF00) >>8;
            ExtraData[2] = (i & 0xFF);

            for (unsigned char j = 0; j < 4; j++)
            {
                unsigned char enable_communication = 0;

                switch(j)
                {
                    case 0:
                        if (parts_get & 0b00000001)
                        {
                            ExtraData[3] = 0;
                            enable_communication = 1;
                        }
                        break;

                    case 1:
                        if (parts_get & 0b00000010)
                        {
                            ExtraData[3] = 1;
                            enable_communication = 1;
                        }
                        break;

                    case 2:
                        if (parts_get & 0b00000100)
                        {
                            ExtraData[3] = 2;
                            enable_communication = 1;
                        }
                        break;

                    case 3:
                        if (parts_get & 0b00001000)
                        {
                            ExtraData[3] = 3;
                            enable_communication = 1;
                        }
                        break;
                }

                if (enable_communication)
                {
                    ret = packACommand(0x41, 0, ExtraData, 5);
                    ret = exchangeDataI2C(&snseAddr, 7);

                    packetDownloadDelay (missionParameters[6]);

                    ret = packACommand(0x21, 0, ExtraData, 5);
                    ret = exchangeDataI2C(&snseAddr, 7);
                    getSecondData = bufferReceive[2];

                    __delay_ms(20);

                    //Sent data to CCU1 by packet
                    ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
                    ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);

                    if (getSecondData == 1)
                    {
                        packetDownloadDelay (missionParameters[6]);

                        ret=packACommand(0x21, 0, ExtraData,5);
                        ret=exchangeDataI2C(&snseAddr, 7);

                        __delay_ms(20);

                        //Sent data to CCU1 by packet
                        ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
                        ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
                    }

                    __delay_ms(100);

                }
            }
        }
    }

    packetDownloadDelay (missionParameters[6]);

    //Sent data to CCU1 by packet
    ret = packACommand(C_TRPACK, 0x00, "END", 3);
    ret = exchangeDataI2C(&missionParameters[0], 5);

    __delay_ms(500);

    adsTurnOff (1);
}

void DLPrealTimeDataRequest (const unsigned char* missionParameters)
{
    uint16_t N_samples = 0;

    N_samples = missionParameters[24];      // Assemble N_samples variable
    N_samples = N_samples << 8;
    N_samples |= missionParameters[23];

    payloadsTurnOnOff (C_SETON, 3);

    // Start DLP Real Time Mode
    ret = packACommand(0x15, 0x00, missionParameters+2, 25);
    ret = exchangeDataI2C(&ecuMAddr, 27);

    __delay_ms(3000);   // DLP configuration waiting

    for (uint16_t i = ((N_samples*12)/60); i != 0 ; --i)
    {
        // Read data packet from DLP
        ret = packACommand(C_OBC_TICK, 0x00, noExtraData, 0);
        ret = exchangeDataI2C(&ecuMAddr, 2);

        __delay_ms(100);

        if (i > 1)
        {
            // Sent data to CCU by packet
            ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }

        else
        {
            // Sent last data to CCU by packet
            ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }

        packetDownloadDelay (missionParameters[1]);

    }

    __delay_ms(1000);

    payloadsTurnOnOff (C_SETOFF, 3);
}

void CPDrealTimeDataRequest (const unsigned char* missionParameters)
{
    payloadsTurnOnOff (C_SETON, 2);

    // Start CPD Real Time Mode (PART 1)
    ret = packACommand(0x12, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);

    __delay_ms(1000);

    // Start CPD Real Time Mode (PART 2)
    ret = packACommand(0x13, 0x00, missionParameters+2, 19);
    ret = exchangeDataI2C(&ecuMAddr, 21);

    __delay_ms(2000);

    for (uint8_t i = 32; i != 0; --i)
    {
        // Read data packet from CPD
        ret = packACommand(C_OBC_TICK, 0x00, noExtraData, 0);
        ret = exchangeDataI2C(&ecuMAddr, 2);

        packetDownloadDelay (missionParameters[1]);

        if (i > 1)
        {
            // Sent data to CCU by packet
            ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }

        else
        {
            // Sent last data to CCU by packet
            ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }

        packetDownloadDelay (missionParameters[1]);

    }

    __delay_ms(1000);

    payloadsTurnOnOff (C_SETOFF, 2);
}

void LiulinRealTimeDataRequest (const unsigned char* missionParameters)
{
    payloadsTurnOnOff (C_SETON, 2);

    // Start LIULIN Real Time Mode (PART 1)
    ret = packACommand(0x12, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);

    __delay_ms(1000);

    // Start LIULIN Real Time Mode (PART 2)
    ret = packACommand(0x14, missionParameters[1], noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);

    __delay_ms(500);

    for (uint8_t j = missionParameters[1]; j != 0; --j)
    {
        for (uint8_t i = (missionParameters[2] * 5); i != 0; --i)
        {
            __delay_ms(1000);
        }
        
        for (uint8_t i = 9; i != 0 ; --i)
        {
            // Read data packet from LIULIN
            ret = packACommand(C_OBC_TICK, missionParameters[1], noExtraData, 0);
            ret = exchangeDataI2C(&ecuMAddr, 2);

            packetDownloadDelay (missionParameters[3]);

            // Sent data to CCU by packet
            ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);

            __delay_ms(100);
        }
    }
    
    // Read last data packet from LIULIN
    ret = packACommand(C_OBC_TICK, missionParameters[1], noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);
    
    packetDownloadDelay (missionParameters[3]);
    
    // Sent last data to CCU by packet
    ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
    ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);

    payloadsTurnOnOff (C_SETOFF, 2);
}

void materialRealTimeDataRequest (const unsigned char* missionParameters)
{
    payloadsTurnOnOff (C_SETON, 1);     // Set ECU On

    __delay_ms(1000);

    // Set Material mission ON
    ret = packACommand(0x05, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);

    __delay_ms(1000);

    ret = packACommand(0x02, 0x00, noExtraData, 0x00);  //Send command for slave to start operating.
    ret = exchangeDataI2C(&matsAddr, 2);

    packetDownloadDelay (missionParameters[2]);

    for (uint8_t i = missionParameters[1] - 1; i != 0; --i)
    {
        ret = packACommand(0x02, 0x00, noExtraData, 0x00);  //Send command for slave to start operating.
        ret = exchangeDataI2C(&matsAddr, 2);

        packetDownloadDelay (missionParameters[2]);

        // Sent data to CCU by packet
        ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
        ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);

        __delay_ms(50);
    }
    
    ret = packACommand(C_OBC_TICK, 0x00, noExtraData, 0x00);  //Send command for slave to start operating.
    ret = exchangeDataI2C(&matsAddr, 2);
    
    packetDownloadDelay (missionParameters[2]);
    
    // Sent last data to CCU by packet
    ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
    ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);

    __delay_ms(50);

    // Set Material mission OFF
    ret = packACommand(0x08, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);

    __delay_ms(500);

    payloadsTurnOnOff (C_SETOFF, 1);
}

void sdCardDataRequest (const unsigned char* missionParameters)
{
    if ((missionParameters[1] == ecuMAddr) || (missionParameters[1] == matsAddr))
    {
        payloadsTurnOnOff (C_SETON, 1);        // Set ECU On
    }

    else if (missionParameters[1] == uecuAddr)
    {
        // Set UCP ON
        ret = packACommand(C_SETON, UCP_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);
    }

    __delay_ms(1000);

    // Send data request to subsystem's SD card
    ret = packACommand(0xAA, 0x01, missionParameters+3, 3);
    ret = exchangeDataI2C(&missionParameters[1], 5);

    __delay_ms(6000);

    for (unsigned char i = missionParameters[2]; i != 0 ; --i)
    {
        if (i > 1)
        {
            //Send data request to EPS
            ret = packACommand(0xAA, 0, noExtraData, 0);
            ret = exchangeDataI2C(&missionParameters[1], 2);

            packetDownloadDelay (missionParameters[6]);

            //Sent data to CCU1 by packet disabling RX
            ret = packACommand(C_TRPACK, 0x01, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }

        else
        {
            //Send data request to EPS
            ret = packACommand(C_OBC_TICK, 0, noExtraData, 0);
            ret = exchangeDataI2C(&missionParameters[1], 2);

            packetDownloadDelay (missionParameters[6]);

            //Sent data to CCU1 by packet enabling RX
            ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
            ret = exchangeDataI2C(&missionParameters[0], bufferReceive[0]+2);
        }
    }

    __delay_ms(1000);

    if ((missionParameters[1] == ecuMAddr) || (missionParameters[1] == matsAddr))
    {
        payloadsTurnOnOff (C_SETOFF, 1);    // Set ECU ON
    }

    else if (missionParameters[1] == uecuAddr)
    {
        // Set UCP ON
        ret = packACommand(C_SETOFF, UCP_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);
    }

    __delay_ms(1000);
}

void directSubsystemCommand (unsigned char* controlCommand)
{
    ret = packACommand(controlCommand[1], controlCommand[2], noExtraData, 0);
    ret = exchangeDataI2C(&controlCommand[0], 2);
}

void monitorMode (unsigned char* controlCommands)
{
    if ((controlCommands[1] == ecuMAddr) || (controlCommands[1] == matsAddr))
    {
        payloadsTurnOnOff (C_SETON, 1);        // Set ECU On
    }

    else if (controlCommands[1] == uecuAddr)
    {
        // Set UCP ON
        ret = packACommand(C_SETON, UCP_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);
    }

    else if (controlCommands[1] == snseAddr)
    {
        adsTurnOn (1);
    }

    __delay_ms(1000);
    
    ret = packACommand(controlCommands[3], controlCommands[4], controlCommands+6, controlCommands[5]);
    ret = exchangeDataI2C(&controlCommands[1], controlCommands[5] + 2);
    
    for (uint8_t i = 2; i != 0; --i)
    {
        __delay_ms(1000);
    
        ret = packACommand(C_OBC_TICK, 0x00, noExtraData, 0);
        ret = exchangeDataI2C(&controlCommands[1], 2);
        
        __delay_ms(1000);
        
        //Sent data to CCU by packet enabling RX
        ret = packACommand(C_TRPACK, 0x00, bufferReceive+1, bufferReceive[0]);
        ret = exchangeDataI2C(&controlCommands[0], bufferReceive[0]+2);
    }
    
    __delay_ms(1000);
    
    if (controlCommands[2] != 0) { return; }

    if ((controlCommands[1] == ecuMAddr) || (controlCommands[1] == matsAddr))
    {
        payloadsTurnOnOff (C_SETOFF, 1);    // Set ECU ON
    }

    else if (controlCommands[1] == uecuAddr)
    {
        // Set UCP ON
        ret = packACommand(C_SETOFF, UCP_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);
    }

    else if (controlCommands[1] == snseAddr)
    {
        adsTurnOff (1);
    }

    __delay_ms(1000);
}

void resetCounterFunction (void)
{
    resetCounter[1] = eeprom_read(0x00);
    __delay_ms(15);
    resetCounter[0] = eeprom_read(0x01);

    if ((resetCounter[1] & resetCounter[0]) == 0xFF)
    {
        resetCounter[1] = 0x00;
        resetCounter[0] = 0x00;
        eeprom_write(0x00, resetCounter[1]);
        __delay_ms(15);
        eeprom_write(0x01, resetCounter[0]);
        firstBootFlag = 0;
    }

    else if (resetCounter[0] == 0xFF)
    {
        resetCounter[1] += 1;
        resetCounter[0] = 0x00;
        eeprom_write(0x00, resetCounter[1]);
        __delay_ms(15);
        eeprom_write(0x01, resetCounter[0]);
    }

    else
    {
        resetCounter[0] += 1;
        eeprom_write(0x01, resetCounter[0]);
    }

}

void adsTurnOn (const unsigned char mode)
{
    // Set UCP ON
    ret = packACommand(C_SETON, UCP_SW, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);

    __delay_ms(500);

        // Set PICSENSE ON
    ret = packACommand(C_SETON, PIC_SENSE, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);

    __delay_ms(500);
    
    if (mode == 2)
    {
        // Set ADS ON
        ret = packACommand(C_SETON, ADS_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);

         __delay_ms(500);
    }

    // Set UCP OFF
    ret = packACommand(C_SETOFF, UCP_SW, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);

    __delay_ms(500);
}

void adsTurnOff (const unsigned char mode)
{
    if (mode == 2)
    {
        // Set ADS OFF
        ret = packACommand(C_SETOFF, ADS_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);
        __delay_ms(500);
    }

    // Set PICSENSE OFF
    ret = packACommand(C_SETOFF, PIC_SENSE, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);
}

void payloadsTurnOnOff (const unsigned char state, const unsigned mode)       // 1 = PL, 2 = CPD+PL, 3 = DLP+PL, 4 = CPD+DLP+PL
{
    if ((mode == 2) || (mode == 4))
    {
        // Set CPD ON/OFF
        ret = packACommand(state, CPD_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);
        __delay_ms(2000);
    }

    if ((mode == 3) || (mode == 4))
    {
        // Set DLP ON/0FF
        ret = packACommand(state, DLP_SW, noExtraData, 0);
        ret = exchangeDataI2C(&picBAddr, 2);
        __delay_ms(2000);
    }

    // Set PL ON/OFF
    ret = packACommand(state, PL_SW, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);
    
    if (state == C_SETOFF) { return; }

    for (uint8_t i = 20; i != 0; --i)
    {
        __delay_ms(1000);
                
        // Read ECU for check status
        ret = packACommand(C_OBC_TICK, 0x00, noExtraData, 0);
        ret = exchangeDataI2C(&ecuMAddr, 2);
        
        //if ((bufferReceive[1] == 0x45) && (bufferReceive[2] == 0x43)) { return; }
        if ((bufferReceive[1] != 0x00) && (bufferReceive[2] != 0x00)) { return; }
    } 
    
    // Set PL OFF
    ret = packACommand(C_SETOFF, PL_SW, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);

    __delay_ms(1000);

    // Set PL ON
    ret = packACommand(C_SETON, PL_SW, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);

    for (uint8_t i = 20; i != 0; --i)
    {
        __delay_ms(1000);

        // Read ECU for check status
        ret = packACommand(C_OBC_TICK, 0x00, noExtraData, 0);
        ret = exchangeDataI2C(&ecuMAddr, 2);

        //if ((bufferReceive[1] == 0x45) && (bufferReceive[2] == 0x43)) { return; }
        if ((bufferReceive[1] != 0x00) && (bufferReceive[2] != 0x00)) { return; }
    } 

}

//////// save_log_to_eeprom(event) ///////////////
//96-255 addresses in EEPROM for saving 40 events by 4 bytes:
//Time has 3 bytes (min, hour, day), an event has 1 byte)
void save_log_to_eeprom(unsigned char event)
{
    if (addr_log_eeprom==0)
    {
        for (unsigned int p = 0; p < 40; p++)
        {
            addr_log_eeprom = 4*p + 96;

            if (eeprom_read(addr_log_eeprom) == 0xBB) {break;}
        }
    }

    eeprom_write(addr_log_eeprom, currentTime_pic[1]);     //min
    __delay_ms(15);
    eeprom_write(addr_log_eeprom + 1, currentTime_pic[2]);   //hour
    __delay_ms(15);
    eeprom_write(addr_log_eeprom + 2, currentTime_pic[3]);   //day
    __delay_ms(15);
    eeprom_write(addr_log_eeprom + 3, event);
    __delay_ms(15);

    if (addr_log_eeprom == 252) {addr_log_eeprom = 96;}

    else { addr_log_eeprom += 4;}

    eeprom_write(addr_log_eeprom, 0xBB);
    //print_hex_byte(eeprom_read(addr_log_eeprom));uartWriteBytes("\n\r",2);
}

///////// read_log_from_eeprom() ////////////////////
// The function returns 1 of 3 saved log data in EEPROM. Each packet cantains 56 bytes (14 events by 4 bytes)
// Packet 0 will start with event with next 4 bytes: 0xBB and oldest log data (hour, day, event). Min is rewrited by flag byte 0xBB
// Packet 1 will containe 14 events with time
// Packet 2 will containe 12 lates events and 2 events at the end will be similar to first 2 events in Packet 0 (to keep 56 bytes Packet size)
unsigned char read_log_from_eeprom(unsigned char packet_no)
{
    if (addr_log_eeprom == 0)
    {
        return -1;          //log data wasn't initialized
    }

    for (unsigned char p = 0; p < 56; p++)
    {
        if ((addr_log_eeprom + p + packet_no * 56) < 256)
        {
            bufferReceive[p] = eeprom_read(addr_log_eeprom + p + packet_no * 56);
        }

        else
        {
            bufferReceive[p] = eeprom_read(addr_log_eeprom + p + packet_no * 56 + 96 - 256);
        }
    }

    return 0;
}

void logDataRequest(unsigned char* missionParameters)
{
    for (uint8_t i = 0; i < 3; i++)
    {
        read_log_from_eeprom(i);

        if (i < 2)
        {
            ret = packACommand(C_TRPACK, 0x01, bufferReceive, 56);
            ret = exchangeDataI2C(&missionParameters[0], 58);
        }

        else
        {
            //Sent data to CCU1 by packet enabling RX
            ret = packACommand(C_TRPACK, 0x00, bufferReceive, 56);
            ret = exchangeDataI2C(&missionParameters[0], 58);
        }

        packetDownloadDelay (missionParameters[1]);
    }
}

void packetDownloadDelay (unsigned char timeStep)
{
    __delay_ms(500);

    for (uint8_t w = timeStep; w != 0  ; --w)
        {
            __delay_ms(500);
        }
}

void cpdNormalMode (unsigned char* missionParameters)
{
    payloadsTurnOnOff (C_SETON, 2);
    
    // Set SD memory automatic mode
    ret = packACommand(0x0C, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);
    
    __delay_ms(500);

    // Start CDP and DLP mission with parameters
    ret = packACommand(0x03, missionParameters[0], missionParameters+1, 24);
    ret = exchangeDataI2C(&ecuMAddr, 26);
}

void dlpNormalMode (unsigned char* missionParameters)
{
    payloadsTurnOnOff (C_SETON, 3);
    
    // Set SD memory automatic mode
    ret = packACommand(0x0C, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);
    
    __delay_ms(500);

    // Start DLP mission with parameters
    ret = packACommand(0x04, missionParameters[0], missionParameters+1, 24);
    ret = exchangeDataI2C(&ecuMAddr, 26);
}

void cpdAndDlpNormalMode (unsigned char* missionParameters)
{
    payloadsTurnOnOff (C_SETON, 4);
    
    // Set SD memory automatic mode  
    ret = packACommand(0x0C, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&ecuMAddr, 2);
    
    __delay_ms(500);

    // Start CDP and DLP mission with parameters
    ret = packACommand(0x06, missionParameters[0], missionParameters+1, 24);
    ret = exchangeDataI2C(&ecuMAddr, 26);
}

void materialNormalMode (void)
{
    payloadsTurnOnOff (C_SETON, 1);     // Set ECU On

    __delay_ms(500);

    // Start material mission normal mode 
    ret = packACommand(0x01, 0x00, currentTime_pic, 6);  
    ret = exchangeDataI2C(&matsAddr, 8);
}

void ucpNormalMode (void)
{
    // Set UCP ON
    ret = packACommand(C_SETON, UCP_SW, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);
    
    __delay_ms(500);
    
    // Start UCP mission normal mode
    ret = packACommand(0x55, 0x01, noExtraData, 0);
    ret = exchangeDataI2C(&uecuAddr, 2);   
}

void thermalSwitchNormalMode (void)
{
    // Set UCP ON
    ret = packACommand(C_SETON, UCP_SW, noExtraData, 0);
    ret = exchangeDataI2C(&picBAddr, 2);
    
    __delay_ms(500);
    
    // Start UCP mission normal mode
    ret = packACommand(0x70, 0x00, noExtraData, 0);
    ret = exchangeDataI2C(&uecuAddr, 2);   
}

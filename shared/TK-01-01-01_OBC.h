/* Ten-Koh TK-01-01-01 OBC PCB header file, which defines all the pins of the two
 * included PICs: MCU (master control unit) and BCU (backup control unit).
 * Intended for use with PIC16F877 and version of the PCB newer than 1.1.6.
 */
#ifndef TK_01_01_01
#define	TK_01_01_01

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <htc.h>
#include "spi_master.h"
#include "spi_pins.h"
//#include "uart.h"
#include "i2c_master.h"
//#include "sd.h"
#include "adc.h"
#include "DS1340.h" // RTC lib.
#include "timing.h"
#include "OnBoardCommands.h"
#include "I2CAddresses.h"

/*
 *******************************************************************************
 *  Pin definitions.
 *******************************************************************************
 */
// SPI Master pins and initialization registers.
#define SPI_MOSI_MASTER RB3 // OUT = MOSI - master out slave in. Data to slave
#define SPI_MISO_MASTER RB1 // IN = MISO - master in slave out. Data from slave
#define SPI_CK_MASTER   RB2 // OUT = SCK - SPI clock
#define SPI_MOSI_MASTER_INIT TRISB3
#define SPI_MISO_MASTER_INIT TRISB1
#define SPI_CK_MASTER_INIT TRISB2

// SPI chip selects for other subsystems.
#define SPI_BUS_CS_EPS RA5 // Both EPS PICs in TK-02-05-01.
#define SPI_BUS_CS_COMM1 RA4 // Comms. line 1 in TK-03-04-01.
#define SPI_BUS_CS_COMM2 RA3 // Comms. line 2 in TK-03-04-01.
#define SPI_BUS_CS_EPS_INIT TRISA5
#define SPI_BUS_CS_COMM1_INIT TRISA4
#define SPI_BUS_CS_COMM2_INIT TRISA3

// Interrupts routed to the OBC.
#define GPIO_OBC_EPS RB0 // From TK-02 EPS.
#define GPIO_OBC_COMM1 RB4 // From one line of telecommunications in TK-03.
#define GPIO_OBC_COMM2 RB5 // From the other line of telecommunications in TK-03.
#define GPIO_OBC_EPS_INIT TRISB0
#define GPIO_OBC_COMM1_INIT TRISB4
#define GPIO_OBC_COMM2_INIT TRISB5

// MCU-BCU interface.
#define MCU_BCU_RESET RD4 // Used by the MCU to reset the BCU and vice-versa.
#define UART_RX RC7 // Rx on MCU, Tx on BCU.
#define UART_TX RC6 // Vice-versa, Tx on MCU.
#define MCU_BCU_RESET_INIT TRISD4
#define UART_RX_INIT TRISC7
#define UART_TX_INIT TRISC6

// System-wide I2C, using the in-built PIC module.
#define I2C_SCL RC3
#define I2C_SDL RC4
#define I2C_SCL_INIT TRISC3
#define I2C_SDL_INIT TRISC4

// Misc. internal OBC pins.
#define RTC_I2C_ENABLE RD0 // Pull high to enable RTC level shifter.
#define SD_SPI_ENABLE RD1 // Pull high to enable SD card level shifter.
#define RTC_I2C_ENABLE_INIT TRISD0
#define SD_SPI_ENABLE_INIT TRISD1

// CS for the OBC SD card is actually defined in the
// SD sd_communication_information_t struct.
#define SPI_BUS_CS_SD RD2
#define SPI_BUS_CS_SD_INIT TRISD2

// CS for the OBC temperature measurement ADC is actually defined in the
// adc_communication_information_t struct.
#define SPI_BUS_CS_ADC RD3
#define SPI_BUS_CS_ADC_INIT TRISD3

// Used to disconnect MISO of the SPI_BUS from the internal OBC SPI. Connecting
// them might disrupt the SD card. Raise MISO_SEL high to disconnect internal
// MISO from all the subsystems connected to SPI_BUS.
#define MISO_SEL RD6
#define MISO_SEL_INIT TRISD6

// Choose which PIC, MCU or BCU, is connected to all OBC I/Os using the ADG744
// MUXes. If PIC_SEL of MCU or the BCU is 1, BCU is connected to the OBC outputs.
// If the OR is 0, the MCU is connected.
#define PIC_SEL RD5
#define PIC_SEL_INIT TRISD5

/*
 *******************************************************************************
 *  PIC configuration.
 *******************************************************************************
 */
#define ANALOGUE_PIN_SETUP 0x06 // Use this with ADCON1 to set RE<0:2>, RA0, RA1,
    // RA2, RA3 and RA5 to digital, as needed by this PIC.
#define UART_BAUD 9600 // Bits per second.
#define MAIN_LOOP_WAIT 500 // How long to wait after every main loop.
#define _XTAL_FREQ 20000000 // 20 MHz crystal.

/*
 *******************************************************************************
 *  Variable declarations.
 *******************************************************************************
 */
// Transmit and receive buffers that we'll use to send data to and from the
// slave PICs on I2C and SPI (in case of emergency and only for comms and EPS).
#define RECEIVE_BUF_SIZE 75 // In bytes. Max. comms packet length is 96 bytes.
#define TRANSMIT_BUF_SIZE 75  // In bytes. Keep both the same length to be able
                             // move data around quickly and easily.

//sd_communication_information_t obcSDCard;
adc_communication_information_t tempADC;
uint16_t currentTemp[2]={0}; // Current temperatures of the MCU and BCU, respectively.

// Current time read from the RTC. Will be reset to this value at start-up.
//unsigned char currentTime[7]={0x55,0x59,0x23,0x31,0x12,0x00,5}; //0,4,8,12, ...years are leap-years

uint8_t ret = 0; // Current return code of the most recent function.
unsigned char noExtraData[1] = {0}; // Extra data to pack into a command that has no extraData.

unsigned char bufferTransmit[TRANSMIT_BUF_SIZE]; //@ 0x0110;
unsigned char bufferReceive[RECEIVE_BUF_SIZE]; //@ 0x0190;

unsigned char currentCommand[43] = {0};
unsigned char firstBootFlag = 0xFF;
unsigned char beaconCounter = 0;
unsigned char ccuBeaconFlag = 0x30;
unsigned char commFlag = 0;
unsigned char ucpFlag = 0;
unsigned char telemetryFlag = 0;
unsigned char currentMode = 0;
unsigned char resetCounter[2];
uint16_t currentMissionTime = 0;
uint16_t missionStartTime = 0;
uint16_t partialTime_1 = 0;
uint16_t partialTime_2 = 0;
uint16_t missionFinishTime = 0;
uint16_t adsTime = 0;
unsigned int addr_log_eeprom = 0;

unsigned int adsInitFlag = 0;

/*
 *******************************************************************************
 *  Function declarations.
 *******************************************************************************
 */
uint8_t measureTemperature(uint16_t* data);
uint8_t readCommPins(void);
uint8_t exchangeDataI2C(unsigned char* const slaveAddr, uint8_t noBytes);
uint8_t exchangeDataSPI(uint8_t slaveIndex, uint8_t noBytes);
uint8_t packACommand(unsigned char commandByte, unsigned char suppDataByte,
        unsigned char* extraData, unsigned char extraDataSize);
uint8_t receiveGScommand (const unsigned char* ccuAddress);
void requestTelemetryPacket (const unsigned char* missionParameters);
void IFPVrealDataRequest (const unsigned char* missionParameters);
void EPSrealDataRequest (const unsigned char* missionParameters);
void IFPVsdCardDataRequest (const unsigned char* missionParameters);
void DLPrealTimeDataRequest (const unsigned char* missionParameters);
void CPDrealTimeDataRequest (const unsigned char* missionParameters);
void LiulinRealTimeDataRequest (const unsigned char* missionParameters);
void materialRealTimeDataRequest (const unsigned char* missionParameters);
void sdCardDataRequest (const unsigned char* missionParameters);
//void adsLastSDSavedSector (const unsigned char missionParameter);
void directSubsystemCommand (unsigned char* controlCommand);
void sendBeacon (void);
void resetCounterFunction (void);
void adsTurnOn (const unsigned char mode);
void adsTurnOff (const unsigned char mode);
void payloadsTurnOnOff (const unsigned char state, const unsigned mode);
void save_log_to_eeprom (unsigned char event);
unsigned char read_log_from_eeprom(unsigned char packet_no);
void logDataRequest(unsigned char* missionParameters);
void packetDownloadDelay (unsigned char timeStep);
void cpdNormalMode (unsigned char* missionParameters);
void dlpNormalMode (unsigned char* missionParameters);
void cpdAndDlpNormalMode (unsigned char* missionParameters);
void materialNormalMode (void);
void monitorMode (unsigned char* controlCommands);
void ucpNormalMode (void);
void thermalSwitchNormalMode (void);

#endif	/* XC_HEADER_TEMPLATE_H */

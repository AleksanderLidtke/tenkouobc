#include "debugFunctions.h"

uint8_t writeSDTestData(void)
/* Record known data in the SD card in bytes 0 to 3 in blocks 1 to 5 inclusive.
 * The test data are ABCDEFGHIJKLMNOPQRST */
{
    _delay(5000); // 1 ms for power on
    _delay(500); // 0.1 ms for power ramp up
    _delay(5000); // 1 ms for Initialization delay
    
    sd_setup_SPImode(&obcSDCard);
    
    // Write known data to the card.
    for(uint32_t address = 1 ; address < 6 ; address++){
        obcSDCard.block_number = address;
        switch(address){
            case 1:
                Write_SDcard(&obcSDCard,tx_data_1); break;
            case 2:
                Write_SDcard(&obcSDCard,tx_data_2); break;
            case 3:
                Write_SDcard(&obcSDCard,tx_data_3); break;
            case 4:
                Write_SDcard(&obcSDCard,tx_data_4); break;
            case 5:
                Write_SDcard(&obcSDCard,tx_data_5); break;
        }
    }
    return 0;
}

uint8_t readSDTestData(unsigned char* data)
/* Read the data from the SD card in bytes 0 to 3 in blocks 1 to 5 inclusive.
 * The test data are ABCDEFGHIJKLMNOPQRST */
{
    _delay(5000); // 1 ms for power on
    _delay(500); // 0.1 ms for power ramp up
    _delay(5000); // 1 ms for Initialization delay
    
    sd_setup_SPImode(&obcSDCard);
    
    for(uint32_t address = 1 ; address < 6 ; address++)
    {
        obcSDCard.block_number = address;
        Read_SDcard(&obcSDCard,data+(address-1)*4); // Increment the pointer in steps of 4.
    }
    
    return 0;
}

uint8_t test_sd_communication(void)
/* Record known data in the SD card, read it and transmit on UART to check
 * if they make sense and the SD card works. Assume that the SPI pins are
 * already configured as inputs and outputs. */
{
    ret = writeSDTestData(); // Record known data in the SD card.
    uint8_t sdData[20]={0}; 
    ret = readSDTestData(sdData); // Read the SD test data.
    uartWriteBytes(sdData,20); // Send for verification.
    
    return 0;
}

uint8_t printCurrentTemperatures(uint16_t* tempData)
/* Print the current MCU and BCU temperatures in degrees C to UART. */
{
    int status; // Float to char array cast status.
    char* buf; // float will be cast into this.
	float temp=0.; // Temp. temperature in deg C.
	
    uartWriteBytes(uartTerminator,1); // Make it clear where the test output begins.
    
	for(uint8_t i=0; i<2; i++)
	{
		temp=adc_conversion_voltage(tempData[i],DOUBLE_RANGE); // ADC reading in V.
        // Scale temperatures[i] to degrees C. We use 6.8k resistance in series
        // with the transducer and the nominal output is 1 uA/K. The current at
        // 25 C is 298.2 uA i.e. 273.2 uA at 0 C.
        temp=temp/6800.*1000000.-273.2; // I=U/R, T=I/(1uA/K)
		// Print temperature to UART.
        buf=ftoa(temp,&status);
        uartWriteBytes(buf,5);
        uartWriteBytes(uartTerminator,1); // Show a separator between&after readings.
	}
	
	return 0;
}


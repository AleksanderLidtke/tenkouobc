/* Debugging functions for the TK-01-01-01 OBC PCB. */
#ifndef OBC_DEBUG_FUNCTIONS
#define OBC_DEBUG_FUNCTIONS

#include "TK-01-01-01_OBC.h" // All the necessary includes.

unsigned char uartTerminator[] = {"XXX\n"}; // Distinct end of debug messages.

uint8_t tx_data_1[4] = {'A','B','C','D'}; // SD card test data.
uint8_t tx_data_2[4] = {'E','F','G','H'};
uint8_t tx_data_3[4] = {'I','J','K','L'};
uint8_t tx_data_4[4] = {'M','N','O','P'};
uint8_t tx_data_5[4] = {'Q','R','S','T'};

uint8_t test_sd_communication(void);
uint8_t writeSDTestData(void);
uint8_t readSDTestData(unsigned char* data);
uint8_t printCurrentTemperatures(uint16_t* tempData);

#endif // OBC_DEBUG_FUNCTIONS

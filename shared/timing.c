#include "timing.h"

//Interrup from timer0. Increments a counter for 1sec.
static void interrupt timer_isr()
{  
    if(TMR0IF==1)
    {        
        TMR0 = 61;     /*Load the timer Value, (Note: Timervalue is 101 instaed of 100 as the
                          TImer0 needs two instruction Cycles to start incrementing TMR0 */
        TMR0IF=0;       // Clear timer interrupt flag
        TMR0_count_1sec++;
    } 
}

//start timer0 to interrut every 10ms
//Reset counters for 1sec and 60sec
static void timer0_init(void)
{
    //Timer0 Registers Prescaler= 256 - TMR0 Preset = 61 - Freq = 100.16 Hz - Period = 0.009984 seconds
    T0CS = 0;  // bit 5  TMR0 Clock Source Select bit...0 = Internal Clock (CLKO) 1 = Transition on T0CKI pin
    T0SE = 0;  // bit 4 TMR0 Source Edge Select bit 0 = low/high 1 = high/low
    PSA = 0;   // bit 3  Prescaler Assignment bit...0 = Prescaler is assigned to the Timer0
    PS2 = 1;   // bits 2-0  PS2:PS0: 1/256
    PS1 = 1;
    PS0 = 1;
    TMR0 = 61;
    TMR0IE = 1;
    GIE=1;          //Enable Global Interrupt
    PEIE=1;         //Enable the Peripheral Interrupt
    TMR0_count_1sec = 0;
}

static void read_time_from_eeprom(void)
{
    //sec
    currentTime_pic[0] = 0;                 //sec
    //min
    currentTime_pic[1] = 0;                 //min, it will be 0 if min value has never been saved to eeprom
    
    eeprom_addr_for_min = 6;                //default address for saving mins value (in case if data there is 0xAA, or 0xAA wan't found in other addresses)
    
    if (eeprom_read(6) == 0xAA)
    {
        currentTime_pic[1] = eeprom_read(205);
    }
    else
    {
        for (unsigned char i = 7; i < 206; i++)
        {
            if (eeprom_read(i) == 0xAA)
            {
                currentTime_pic[1] = eeprom_read(i-1);
                eeprom_addr_for_min = i;
                break;
            }            
        }
    }
    
    //[2] - hour, [3] - day, [4] - month, [5] - year
    for (unsigned char i = 2; i < 6; i++)
    {
        currentTime_pic[i] = eeprom_read(i);
    }
}

//////////////////// save_time_to_EEPROM_every_one_min()/////////////////
// It checks if min value in currentTime_pic = to last saved min value in EEPROM.
// If not, then the min value will be saved in EEPROM
static void save_time_to_EEPROM_every_one_min(void)
{
    if (currentTime_pic[1]!=current_min)
    {//(TMR0_count_60sec>=6000){     //1sec = 100; 60sec = 6000
        current_min = currentTime_pic[1];
        
        if (eeprom_addr_for_min==95)
        {   //205 - 200 bytes, 95 - 90 bytes for saving minutes
            eeprom_write(eeprom_addr_for_min,currentTime_pic[1]);
            __delay_ms(10);
            eeprom_write(0x06, 0xAA);
            eeprom_addr_for_min = 6;
        }
        
        else
        {
            eeprom_write(eeprom_addr_for_min,currentTime_pic[1]);
            __delay_ms(10);
            eeprom_write(eeprom_addr_for_min+1, 0xAA);
            eeprom_addr_for_min++;
        }
        
        for (unsigned char u = 2; u < 6; u++)
        {
            if (currentTime_pic[u] != eeprom_read(u))
            {
                __delay_ms(10);
                eeprom_write(u,currentTime_pic[u]);             
            }
        }
    }
}

/////////////// set_init_time_to_RTC() //////////
//Conver time in hex (currentTime_pic) to RTC format time (currentTime)
//Set currentTime to RTC
void set_time_to_RTC(void)
{    
    for(unsigned char i = 0; i < 6; i++)
    {
        a = currentTime_pic[i]/10;
        b = currentTime_pic[i]%10;
        currentTime[i] = ((a<<4) & 0xf0) + b;
    }    
    
    RTC_I2C_ENABLE=1;
    ret=(uint8_t) setRTC(currentTime); // Initialise the RTC time.
    RTC_I2C_ENABLE=0;
}

/////////////// init_timing() ///////////////////
//It is assumed that Time and date was set from GS to real time at least once (can be done before launch).
//The function reads outputs from RTC and checks if it was reseted or replies though I2C.
//Call function read_time_from_eeprom() to find addres 
//Check if year==0x00 or year==0xff, then need set time from EEPROM to RTC
////0x00 - RTC was reseted (starts counting years from 0).
////0xff - no relpy through I2C.
//In other case RTC wasn't reseted and will continue counting.
//Time in EEPROM will be updated later in timing_one_sec() function.
void init_timing(void)
{
    //Read output from RTC
    RTC_I2C_ENABLE = 1;    
    ret=(uint8_t) readRTC(currentTime); // Get a new time vector from the RTC.
    RTC_I2C_ENABLE = 0;
    
    read_time_from_eeprom();            //For finding an address in EEPROM for saving new value of minutes
    
    if (currentTime[5] == 0x00 || currentTime[5] == 0xFF)
    {
        set_time_to_RTC();
    }
    
    timer0_init();                      //start timer0 to interrut every 10ms
}

///////////////// timing_one_sec() ////////////////////
// It waits till 1 second counted by timer0 interrupts (TMR0_count_1sec=100).
// Read time from RTC and convert it to hex format.
// Check in RTC works. If yes, then update currentTime_pic by RTC time,
// in other case update it by pic counter
// Calls function save_time_to_EEPROM_every_one_min() to save currentTime_pic to EEPROM 
void timing_one_sec (void)
{
    while(TMR0_count_1sec < 100); //1 sec
    TMR0_count_1sec = 0; 
    
    // Get the current time.
    RTC_I2C_ENABLE = 1;    
    ret=(uint8_t) readRTC(currentTime); // Get a new time vector from the RTC.
    RTC_I2C_ENABLE = 0;  
    
    a = currentTime[0] & 0x0F;
    b = ((currentTime[0] >> 4) & 0x0F)*10;
    seconds = a + b;
    if (currentTime[0] != 0xFF && (seconds - currentTime_hex[0] != 0)){
        for(unsigned char i = 0; i < 6; i++)
        {
            a = currentTime[i] & 0x0f;
            b = ((currentTime[i]>>4) & 0x0f)*10;
            currentTime_hex[i] = a + b;
            currentTime_pic[i] = currentTime_hex[i];
        }
    }
    else
    {
        if (currentTime_pic[5]%4 == 0) { month_days[1] = 30;} //Feb, 29+1
        
        else {month_days[1] = 29;} //Feb, 28+1

        month_day_temp = month_days[currentTime_pic[4]-1];

        currentTime_pic[0] += 1;                            //sec
        currentTime_pic[1] += currentTime_pic[0]/60;        //min
        currentTime_pic[2] += currentTime_pic[1]/60;        //hour
        currentTime_pic[3] += currentTime_pic[2]/24;        //day
        currentTime_pic[4] += currentTime_pic[3]/month_day_temp;//month
        currentTime_pic[5] +=  currentTime_pic[4]/13;        //year

        currentTime_pic[0] %= 60;   
        currentTime_pic[1] %= 60;
        currentTime_pic[2] %= 24;
        currentTime_pic[3] %= month_day_temp;
        
        if (currentTime_pic[3] == 0) {currentTime_pic[3] = 1;}
        
        currentTime_pic[4] %= 13;
        
        if (currentTime_pic[4] == 0) {currentTime_pic[4] = 1;}
    }
    
    save_time_to_EEPROM_every_one_min();
    
}

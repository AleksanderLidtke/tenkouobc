/* 
 * File:   timing.h
 * Author: Dmytro Faizullin
 *
 * Created on 28 June 2018, 13:37
 */

#ifndef TIMING_H
#define	TIMING_H

#include "TK-01-01-01_OBC.h"

/////////////// How to use in main function/////////////////
// 1. Call init_timing() before while loop
// 2. Use timing_one_sec() in while loop to keep one sec operation
// 3. Use set_time_to_RTC() to update RTC value by currentTime_pic[]
////////////////////////////////////////////
unsigned char currentTime_pic[7] = {55,59,23,31,12,0,5};
unsigned char eeprom_addr_for_min = 0;
unsigned char currentTime[7] = {0,0,0,0,0,0,0}; //0,4,8,12, ...years are leap-years
unsigned char currentTime_hex[6] = {0};
unsigned char month_days[12] = {31,28,31,30,31,31,30,31,30,31,30,31};
unsigned char a,b,seconds,month_day_temp;
unsigned char current_min = 0xBB;
unsigned int TMR0_count_1sec = 0;

void init_timing(void);
void timing_one_sec (void);
void set_time_to_RTC(void);

#endif